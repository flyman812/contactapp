import 'package:contactapp/models/info_details.dart';
import 'package:flutter/foundation.dart';
import 'package:hive/hive.dart';

part 'contact.g.dart';

@HiveType(typeId: 0)
class Contact {
  @HiveField(0)
  List<InfoDetails>? fieldsDetails;
  @HiveField(1)
  int? checkedCount;

  Contact({
    this.checkedCount,
    this.fieldsDetails,
  });

  Contact.fromJson(Map<String, dynamic> json) {
    try {
      fieldsDetails = List.from(json["fields"].map((e) => Contact.fromJson(e)));
      checkedCount = json["checkedCount"];
    } on Exception catch (e) {
      debugPrint(e.toString());
    }
  }

  Map<String, dynamic> toJson() {
    return {
      "fields": List.from(fieldsDetails!.map((e) => e.toJson())).toString(),
      "checkedCount": checkedCount,
    };
  }
}
