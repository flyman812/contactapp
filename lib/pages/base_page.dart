import 'package:flutter/material.dart';

class BasePage extends StatelessWidget {
  final Widget? body;
  const BasePage({
    Key? key,
    this.body,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        extendBody: false,
        extendBodyBehindAppBar: false,
        body: body,
      ),
    ));
  }
}
