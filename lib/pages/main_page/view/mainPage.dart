import 'package:contactapp/pages/base_page.dart';
import 'package:contactapp/pages/main_page/controller/main_page_controller.dart';
import 'package:contactapp/widgets/inputs/app_input.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MainPage extends StatelessWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MainPageController controller = Get.put(MainPageController());
    return GetBuilder<MainPageController>(
        init: controller,
        builder: (_) {
          return BasePage(
            body: Column(
                children: [
                  AppInput(
                    hintText: "جستجوی نام فرد",
                    hasElevation: false,
                    padding: const EdgeInsets.symmetric(
                        horizontal: 21.0, vertical: 16.0),
                    prefixIcon: Icons.search,
                    textEditingController: _.controller,
                    textDirection: TextDirection.rtl,
                  ),
                ],
              ),
          );
        });
  }
}
