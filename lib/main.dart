import 'package:contactapp/config/themes.dart';
import 'package:contactapp/pages/main_page/view/mainPage.dart';
import 'package:contactapp/services/storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

void main() async{
  await AppStorage.initial();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Contacts App',
      debugShowCheckedModeBanner: false,
      textDirection: TextDirection.ltr,
      theme: AppThemes.main(),
      home: const MainPage(),
    );
  }
}
