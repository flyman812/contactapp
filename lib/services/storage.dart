import 'dart:io';

import 'package:contactapp/config/globals.dart';
import 'package:contactapp/models/info_details.dart';
import 'package:contactapp/pages/add_contact_page/model/contact.dart';
import 'package:hive_flutter/adapters.dart';

class AppStorage {
  AppStorage._();

  static Box<Contact>? _database;

  static Future<void> initial() async {
    await Hive.initFlutter().then((value) async {
      _database = await Hive.openBox('contacts');
    });
  }

  static Future<void> write(Contact contact) async {
    AppGlobals.contacts!.add(contact);
    await _database!.add(contact);
  }

  static Future<List<Contact>> readAll() async {
    return _database!.values.toList();
  }

  static Future<Contact?> read({int? index, Contact? contact}) async {
    if (contact != null) {
      return _database!.getAt(await indexOfContact(contact));
    } else {
      return _database!.getAt(index!);
    }
  }

  static Future<void> update(int index, Contact contact) async {
    AppGlobals.contacts![index] = contact;
    await _database!.putAt(index, contact);
  }

  static Future<void> delete(int index) async {
    await _database!.deleteAt(index);
  }

  static Future<int> indexOfContact(Contact contact) async {
    if (AppGlobals.contacts != null) {
      int indexInGlobals =
          AppGlobals.contacts!.indexWhere((element) => element == contact);
      return indexInGlobals;
    } else {
      List<Contact> contacts = _database!.values.toList();
      int indexInGlobals = contacts.indexWhere((element) => element == contact);
      return indexInGlobals;
    }
  }
}
