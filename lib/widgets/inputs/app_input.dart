import 'package:flutter/material.dart';

class AppInput extends StatelessWidget {
  final String? hintText;
  final IconData? prefixIcon;
  final TextEditingController? textEditingController;
  final bool hasElevation;
  final String? label;
  final TextDirection textDirection;
  final EdgeInsetsGeometry padding;
  const AppInput({
    Key? key,
    this.hintText,
    this.prefixIcon,
    this.textEditingController,
    this.hasElevation = false,
    this.label,
    this.textDirection = TextDirection.ltr,
    this.padding =const EdgeInsets.symmetric(horizontal: 21.0, vertical: 6.0)
  }) : assert((label != null && hintText == null) ||
            (label == null && hintText != null));

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding,
      child: Row(
        mainAxisAlignment: label != null
            ? MainAxisAlignment.spaceBetween
            : MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          label != null
              ? Text(
                  label!,
                  style: const TextStyle(
                    color: Color(0xff747474),
                    fontSize: 12.0,
                  ),
                )
              : const SizedBox(),
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(15.0)),
                color: Colors.white,
                boxShadow: hasElevation
                    ? [
                        const BoxShadow(
                          blurRadius: 20.0,
                        )
                      ]
                    : [],
                border: !hasElevation
                    ? Border.all(
                        color: const Color(0xffCECECE),
                        width: 1,
                      )
                    : null,
              ),
              child: TextFormField(
                controller: textEditingController,
                textDirection: textDirection,
                decoration: InputDecoration(
                  hintText: hintText,
                  hintTextDirection: textDirection,
                  suffixIcon: Icon(
                    prefixIcon,
                    color: const Color(0xff5F5F5F),
                    size: 20.0,
                  ),
                  border: InputBorder.none,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
