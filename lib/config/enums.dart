
enum TypeInfo {
  email,
  address,
  phoneNumber,
  name,
  postalCode,
  file,
}

enum PageState{
  loading,
  loaded,
  initial,
  errorLoading,
}