import 'package:contactapp/config/fonts.dart';
import 'package:flutter/material.dart';

class AppThemes {
  static ThemeData? main() {
    return ThemeData(
      fontFamily: AppFonts.iranYekan,
      scaffoldBackgroundColor:const Color(0xff9D9C9C),
      floatingActionButtonTheme:const FloatingActionButtonThemeData(
        backgroundColor: Color(0xFF57EFDD),
        foregroundColor: Color(0xFF57EFDD),
        elevation: 0.0,
      ),
      primaryColor:const Color(0xFF57EFDD),
      iconTheme:const IconThemeData(
        color: Colors.white,
        size: 18,
      ),
      shadowColor: Colors.black.withOpacity(0.25),
      
    );
  }
}
