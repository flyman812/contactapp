// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'info_details.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class InfoDetailsAdapter extends TypeAdapter<InfoDetails> {
  @override
  final int typeId = 1;

  @override
  InfoDetails read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return InfoDetails(
      label: fields[0] as String?,
      type: fields[2] as TypeInfo?,
      value: fields[1] as String?,
      isChecked: fields[3] as bool?,
      isBookmarked: fields[4] as bool?,
    );
  }

  @override
  void write(BinaryWriter writer, InfoDetails obj) {
    writer
      ..writeByte(5)
      ..writeByte(0)
      ..write(obj.label)
      ..writeByte(1)
      ..write(obj.value)
      ..writeByte(2)
      ..write(obj.type)
      ..writeByte(3)
      ..write(obj.isChecked)
      ..writeByte(4)
      ..write(obj.isBookmarked);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is InfoDetailsAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
