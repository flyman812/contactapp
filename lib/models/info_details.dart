import 'package:contactapp/config/enums.dart';
import 'package:flutter/foundation.dart';
import 'package:hive/hive.dart';

part 'info_details.g.dart';

@HiveType(typeId: 1)
class InfoDetails {
  @HiveField(0)
  String? label;
  @HiveField(1)
  String? value;
  @HiveField(2)
  TypeInfo? type;
  @HiveField(3)
  bool? isChecked;
  @HiveField(4)
  bool? isBookmarked;

  InfoDetails({
    this.label,
    this.type,
    this.value,
    this.isChecked,
    this.isBookmarked,
  });

  InfoDetails.fromJson(Map<String, dynamic> json) {
    try {
      label = json["label"];
      value = json["value"];
      isChecked = json["isChecked"];
      isBookmarked = json["isBookmarked"];
      type = json["type"] == null
          ? null
          : json["type"] == "email"
              ? TypeInfo.email
              : json["type"] == "name"
                  ? TypeInfo.name
                  : json["type"] == "address"
                      ? TypeInfo.address
                      : json["type"] == "file"
                          ? TypeInfo.file
                          : json["type"] == "phone"
                              ? TypeInfo.phoneNumber
                              : TypeInfo.postalCode;
    } on Exception catch (e) {
      debugPrint(e.toString());
    }
  }

  Map<String, dynamic> toJson() {
    return {
      "label": label,
      "value": value,
      "isChecked": isChecked,
      "isBookmarked": isBookmarked,
      "type": type.toString(),
    };
  }
}
